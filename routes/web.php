<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/','PageController@index');
Route::middleware(['auth'])->group(function () {
    Route::resource('panel/user','UserController');
    Route::resource('panel/video', 'VideoController');
    Route::prefix('panel/')->group(function () {
        Route::post('video/upload', ['as' => 'panel.video.upload', 'uses' => 'UploadVideoController@addVideo']);
        Route::get('user/followers', 'PanelUserController@followers');
        Route::get('user/following', 'PanelUserController@following');
    });
    Route::prefix('u/{channel}/')->group(function () {
        Route::get('followers', 'PanelUserController@followers')->name('user.followers');
        Route::get('following', 'PanelUserController@following')->name('user.followings');
        Route::get('follow', ['as' => 'follow.channel', 'uses' => 'ChannelController@follow']);
        Route::get('unfollow', ['as' => 'unfollow.channel', 'uses' => 'ChannelController@unfollow']);
    });
});

Route::resource('/panel/category', 'CategoryController');

Route::prefix('/u/{name}/')->group(function () {
    Route::get('mostview', ['as' => 'channel.mostview', 'uses' => 'ChannelController@mostview']);
    Route::get('/', ['uses' => 'ChannelController@userChannel', 'as' => 'channel']);
    Route::get('mostlike', ['as' => 'channel.mostlike', 'uses' => 'ChannelController@mostLiked']);
});


Route::get('p/{slug}', 'SingleVideoController@show');
Route::get('p/{slug}/download','SingleVideoController@download');
Route::post('p/{slug}/viewplus','SingleVideoController@plusVideo');
Route::get('p/{slug}/like','SingleVideoController@like');
Route::get('p/{slug}/dislike','SingleVideoController@dislike');

/* -------------------Test Routes ---------------*/
Route::prefix('test')->group(function () {
    Route::get('datetime', 'HomeController@dateTime');
    Route::get('arrobj', 'HomeController@arrayObject');
    Route::get('curl','SingleVideoController@testt');
});

Auth::routes();


