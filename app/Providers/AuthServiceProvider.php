<?php

namespace App\Providers;

use App\Channel;
use App\Policies\ChannelPolicy;
use App\Policies\UserPolicies;
use App\User;
use function foo\func;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    Channel::class => ChannelPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //gates for auth
        Gate::define('st_video',function($user,$video){
//            dd($video);
            return $user->id === $video->channel->user_id;
        });
        Gate::define('canLike',function($user,$video){
            // if user does not liked and user logged in
            return !$this->notLiked($user,$video) && $this->userIsLoggedIn();
        });
        Gate::define('userLoggedIn',function(){
            return $this->userIsLoggedIn();
        });
    }

    function notLiked($user, $video)
    {
        $likes = DB::table('likes')
            ->where('user_id', '=', $user->id)
            ->where('video_id', '=', $video->id)
            ->first();
        return $likes ? true : false;
    }

    private function userIsLoggedIn()
    {
        if(auth()->check()){
            return true;
        }
        return false;
    }
}
