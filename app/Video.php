<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'channel_id',
        'slug',
        'title',
        'description',
        'tags',
        'time',
        'video',
        'images',
        'likes',
        'downloads',
        'view_count',
        'can_comment',
        'allowed'
    ];

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public  function  getUserName()
    {
        return $this->channel->user->name;
    }
}
