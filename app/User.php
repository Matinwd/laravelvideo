<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function findChannelByAuthor($name)
    {
         $user = static::where('name','=',$name)->first();
         if(!$user){
             throw new \Exception('User not found');
         }
        return $user->channel()->first();
    }

    public function channel()
    {
        return $this->hasOne(Channel::class);
    }

    public function following()
    {
        return $this->belongsToMany(Channel::class,'follow_channel');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class,'follow_channel');
    }

    public function videolikes()
    {
        return $this->belongsToMany(Video::class,'likes','user_id','video_id');
    }
}
