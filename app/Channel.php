<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public  static function  findChannelByTitle($name)
    {
        return static::all()->where('title','=',$name)->first();
    }

    public function likers()
    {
        return $this->belongsToMany(Channel::class,'likes','user_id','video_id');
    }


}
