<?php

namespace App\Http;
class Flash
{

    public function success($title,$message,$level = 'success')
    {
        $this->create($title, $message, $level);
    }
    public function info($title,$message, $level = 'info')
    {
        $this->create($title, $message,  $level);
    }
    public function error($title,$message,$level = 'error')
    {
        $this->create($title, $message, $level);
    }

    /**
     * @param $title
     * @param $message
     * @param $level
     */
    public function create($title, $message, $level): void
    {
        session()->flash('flash_message',
            ['title' => $title, 'message' => $message, 'level' => $level]
        );
    }
}
