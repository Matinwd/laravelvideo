<?php

namespace App\Http\Controllers;

use ArrayObject;
use DateTime;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function arrayObject()
    {
        $obj = new ArrayObject(['matin','awdno','awdawd','awd']);
        var_dump($obj);
    }

    public function diffBetweenDateTime()
    {
        $date = new DateTime('2019-03-21 21:02');
        $end = new DateTime('2019-03-21 23:00');
        $dd = $date->diff($end);
        dd($dd);
    }
}
