<?php

namespace App\Http\Controllers;

use App\Category;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUrPosts = auth()->user()->channel->videos;
        return view('panelUser.userPosts', compact('allUrPosts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('panelUser.upload', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $video = new Video;
        $video->time = $request->get('time');
        $video->video = $request->get('videoUrl');
        $video->tags = $request->get('tags');
        $video->description = $request->get('description');
        $video->title = $request->get('title');
        $video->channel_id = $request->get('channel_id');
        $video->images = $request->get('imageUrl');
        $video->can_comment = $request->get('comment');
        $video->slug = $this->createSlug();
        // fully mldwmlawd
        // this life
        $video->saveOrFail();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $videos = auth()->user()->channel->videos;
        $video = Video::find($id);
        $this->authorize('st_video',$video);
        return view('panelUser.show', compact(['videos','video']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::find($id);
        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'tags'=>'required',
            'can_comment' => 'required',
            'slug'=>'required'
        ]);
        $this->authorize('st_video',$video);
        $video->title = $request->title;
        $video->description = $request->description;
        $video->tags = $request->tags;

        $video->images = $video->images;
        $video->time = $video->time;
        $video->video = $video->video;

        if ($request->video) {
            $this->deleteOldFiles([$video->images, $video->video]);
            $video->images = $request->images;
            $video->time = $request->time;
            $video->video = $request->video;
        }
        $video->can_comment = $request->can_comment;
        $video->slug = $request->slug;
        $video->update();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete video from db
        $video = Video::all()->find($id);
        if(Gate::denies('st_video',$video)){
            return abort(404,'Not Found');
        }

        //delete image and video

        foreach (['images', 'video'] as $value) {
            $item = $this->getFilePath($video, $value);
            $this->FileDelete($item);
        }

        // delete video from db
        Video::destroy($id);


        return redirect()->back();
    }

    private function createSlug(): string
    {

        $slug = mb_substr(md5(random_int(1000, 12130)),0,8);
        while (Video::where('slug', $slug)->first()) {
            $slug = mb_substr(md5(random_int(1000, 12130)), 0, 6);
        }
        return $slug;
    }

    public function deleteOldFiles(array $arr)
    {
        foreach ($arr as $item){
            $res = unlink(public_path($item));
        }
    }

    /**
     * @param $video
     * @return mixed
     */
    public function getFilePath($item,$type): mixed
    {
        $item = parse_url($item->{$type});
        return $item;
    }

    /**
     * @param $video1
     */
    public function FileDelete($item): void
    {
        File::delete($item);
    }
}
