<?php

namespace App\Http\Controllers;

use App\Channel;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userChannel($name)
    {
        $channel = User::findChannelByAuthor($name);
        $videos = $channel->videos()->orderBy('title')->get();
        return view('main.channel', compact(['channel', 'videos']));
    }

    public function mostView($name)
    {
        $channel = User::findChannelByAuthor($name);
        $videos = $channel->videos()->orderBy('view_count', 'Desc')->latest()->paginate(15);
        return view('main.channel', compact(['videos', 'channel']));
    }

    public function mostLiked($name)
    {
        $channel = User::findChannelByAuthor($name);
        $videos = $channel->videos()->orderBy('likes', 'desc')->latest()->paginate(15);
        return view('main.channel', compact(['videos', 'channel']));
    }


    public function follow($channel)
    {
        $channel = Channel::findChannelByTitle($channel);
        $user = auth()->user();
        //$this->authorize('follow', $channel);

        $this->authorize('follow', $channel);
        // increment followers

        $channel->increment('follow');
        $channel->save();

        $channel_id = $channel->id;
        $user->following()->attach($channel_id);

        return redirect()->back();
    }

    public function unfollow($name)
    {

        $channel = Channel::findChannelByTitle($name);

        $user = auth()->user();
        if (Gate::allows('authorUnfollow', $channel)) {
            $channel_id = $channel->id;
            $user->following()->detach($channel_id);
            $channel->decrement('follow');
            $channel->save();
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
