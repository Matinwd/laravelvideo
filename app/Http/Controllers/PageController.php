<?php

namespace App\Http\Controllers;

use App\Category;
use App\Video;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $allCategories  = Category::all();
        $allVideos = Video::orderBy('created_at','ASC')->with('channel')->get();
        return view('index',compact(['allVideos','allCategories']));
    }
    public function about(){
        return view('about');
    }
}
