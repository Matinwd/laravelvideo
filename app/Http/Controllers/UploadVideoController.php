<?php

namespace App\Http\Controllers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class UploadVideoController extends Controller
{
    protected $ffmpeg = 'C:\\ffmpeg\\bin\\ffmpeg.exe';

    public function addVideo(Request $request)
    {
        $file = $request->file('file');

        //get time of video
        $time = $this->getTimeVideo($file);

        //get image url
        $imageUrl = $this->getImagePath($file);

         //upload video
        $videoUrl = $this->uploadVideo($file);

        return ['imageUrl'=>$imageUrl , 'videoUrl' => $videoUrl,'videoTime' => $time];
    }

    /**
     * @param $file
     * @return string
     */
    private function uploadVideo($file): string
    {
        $randomCreated = substr(md5(rand(123,231213)),0,6);
        $directory = "videos/" . Carbon::now()->year . '/' . Carbon::now()->month . '/' . Carbon::now()->day;
        $newName = time() .  $randomCreated . $file->getClientOriginalName();
        $file->move($directory, $newName);
        $videoUrl = "{$directory}/{$newName}";
        return $videoUrl;
    }

    /**
     * @param string $imageFolder
     */
    private function checkDir(string $imageFolder): void
    {
        if (!is_dir($imageFolder)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($imageFolder, 0755, true);
        }
    }

    /**
     * @param $file
     * @return string
     */
    private function getImagePath($file): string
    {
//take screen shot from video and upload it
        $imageFolder = "images\\" . Carbon::now()->year . '\\' . Carbon::now()->month . '\\' . Carbon::now()->day . '\\';
        //check if "images" folder does not exist make it
        $this->checkDir($imageFolder);
        $publicPathFolder = public_path() .'\\'. $imageFolder;
        $newImageName = rand(10, 10000) . '-' . time() . '.jpg';
        $fileDir = $publicPathFolder . '\\' . $newImageName;

        $imageUrl = $imageFolder . $newImageName;
        $imageUrl = str_replace('\\','/',$imageUrl);

        $cmd = "$this->ffmpeg -i $file -an -ss 10 -s 500*500 $fileDir";
//        return $cmd;
        shell_exec($cmd);
        return $imageUrl;
    }

    /**
     * @param $file
     * @return bool|string
     */
    private function getTimeVideo($file)
    {
        $cmd = "$this->ffmpeg -i $file 2>&1";

        $shell = shell_exec($cmd);

        preg_match('/ Duration: (.*) /',$shell,$match);

        return substr($match[1],0,8);
    }
}
