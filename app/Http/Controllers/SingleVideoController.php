<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SingleVideoController extends Controller
{
    public function show($slug)
    {
        $lastVideos = Video::take(7)->orderBy('created_at')->get();
        $video = $slug;
        $likers = $video->channel->likers;
        return view('video', compact(['lastVideos', 'video','likers']));
    }

    public function download($slug)
    {
        $video = $slug;
        return response()->download($video->video);
        return redirect()->back();
    }


    public function like($slug)
    {
        $video = $slug;
        $user = auth()->user();
        $this->authorize('canLike', $user, $video);
        $user->videolikes()->attach($video);
        $video->increment('likes');
        flash('Liked', 'Liked Successfully!', 'success');
        return redirect()->back();
    }

    public function dislike($slug)
    {
        $this->authorize('userLoggedIn');
        $user = auth()->user();
        $video = $slug;
        if (Gate::denies('canLike', $video)) {
            $user->videolikes()->detach($video);
            $video->decrement('likes');
            flash('Disliked', 'Successfully disliked', 'success');
        }
        return redirect()->back();
    }

    public function plusVideo($slug)
    {
        $video = $slug;
        $video->increment('view_count');
        $video->save();
        return $video->view_count;
    }
}
