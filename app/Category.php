<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['parent_id','name'];

    public function parent()
    {
        return $this->hasOne(__CLASS__);
    }

    public function childrens()
    {
        return $this->hasMany(__CLASS__);
    }
}
