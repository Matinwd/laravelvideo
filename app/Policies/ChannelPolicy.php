<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class ChannelPolicy
{
    use HandlesAuthorization;

    public function follow($user, $channel)
    {
        return !$this->isAuthor($user, $channel) && !$this->isFollowing($user, $channel);
    }

    private function isAuthor($user, $channel)
    {
        return $user->id === $channel->user_id;
    }

    private function isFollowing($user, $channel)
    {
        $f = DB::table('follow_channel')
            ->where('user_id', '=', $user->id)
            ->where('channel_id', '=', $channel->user_id)
            ->first();
        if ($f) {
            return true;
        }
        return false;
    }

    function authorUnfollow($user, $channel)
    {
        return !$this->isAuthor($user, $channel) && $this->isFollowing($user, $channel);
    }

    public function canLike($user, $video)
    {
//        dd('awaf');
        return $this->notLiked($user,$video) && $this->userIsLoggedIn();
    }

    function notLiked($user, $video)
    {
        $likes = DB::table('likes')
            ->where('user_id', '=', $user->id)
            ->where('video_id', '=', $video->id)
            ->first();
        return $likes ? true : false;
    }

    private function userIsLoggedIn()
    {
        if(auth()->check()){
            return true;
        }
        return false;
    }
}
