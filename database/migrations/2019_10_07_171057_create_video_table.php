<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->unsignedBigInteger('channel_id');
            $table->foreign('channel_id')->references('id')->on('channels');

            $table->string('slug');
            $table->string('title');
            $table->text('description');
            $table->text('tags');

            $table->string('time');
            $table->text('video');
            $table->text('images');

            $table->integer('likes')->default(0);
            $table->integer('downloads')->default(0);
            $table->integer('view_count')->default(0);
            $table->boolean('can_comment')->default(true);

            $table->boolean('allowed')->default(false);
            $table->engine = 'InnoDB';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video');
    }
}
