<?php

use App\Channel;
use Illuminate\Database\Seeder;

class ChannelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userIds = \App\User::all()->pluck('id');
        foreach ($userIds as $userId){
            factory(Channel::class)->create([
                'user_id' => $userId
            ]);
        }
    }
}
