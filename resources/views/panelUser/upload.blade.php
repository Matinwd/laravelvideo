@extends('master')
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script>
        $(function () {
            Dropzone.options.videoUpload = {
                maxFilesize: 20, //MB
                paramName: 'file',
                dictDefaultMessage: "drop here",
                success: function (file, response) {
                    console.log(response.videoTime);
                    $("#time").val(response.videoTime);
                    $("#videoUrl").val(response.videoUrl);
                    $("#imageUrl").val(response.imageUrl);
                }
            }
        });

    </script>
    <main role="main" class="container">
        <div class="jumbotron">
            <h1>User Panel</h1>
            <p class="lead">This example is a quick exercise to illustrate how the top-aligned navbar works. As you
                scroll,
                this navbar remains in its original position and moves with the rest of the page.</p>
        </div>
    </main>
    <section id="tabs" class="project-tab">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                               role="tab" aria-controls="nav-home" aria-selected="true">Project Tab 1</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                               role="tab" aria-controls="nav-profile" aria-selected="false">Project Tab 2</a>
                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact"
                               role="tab" aria-controls="nav-contact" aria-selected="false">Project Tab 3</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                             aria-labelledby="nav-home-tab">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="formVid mt-3">
                                        <p class="text-left h3">Upload video here</p>
                                        <form id="videoUpload" action="{{ route('panel.video.upload') }}" method="POST"
                                              class="dropzone">
                                            @csrf
                                        </form>
                                    </div>
                                    @include('auth.errors')
                                </div>
                                <div class="col-md-7">
                                    <div class="formText mt-3">
                                        <form method="post" action="{{ route('video.store') }}" class="form-group">
                                            @csrf
                                            <input type="hidden" class="form-control" id="time" name="time">
                                            <input type="hidden" class="form-control" id="videoUrl" name="videoUrl">
                                            <input type="hidden" class="form-control" id="imageUrl" name="imageUrl">
                                            <input type="hidden" class="form-control" id="channel_id" value="{{ auth()->user()->channel->id }}" name="channel_id">
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" class="form-control" id="title" name="title">
                                            </div>
                                            <div class="form-group">
                                                <label for="description">description</label>
                                                <textarea class="form-control" id="description"
                                                          name="description"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="tags">tags</label>
                                                <input type="text" class="form-control" id="tags" name="tags">
                                            </div>
                                            <div class="form-group">

                                                <label for="Category">Select a category</label>
                                                <select name="category" class="form-control" id="Category">
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="comment">Comment</label>
                                                <select class="form-control" name="comment" id="comment">
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-danger" type="submit">Send</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <table class="table" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Project Name</th>
                                    <th>Employer</th>
                                    <th>Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><a href="#">Work 1</a></td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                </tr>
                                <tr>
                                    <td><a href="#">Work 2</a></td>
                                    <td>Moe</td>
                                    <td>mary@example.com</td>
                                </tr>
                                <tr>
                                    <td><a href="#">Work 3</a></td>
                                    <td>Dooley</td>
                                    <td>july@example.com</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <table class="table" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Contest Name</th>
                                    <th>Date</th>
                                    <th>Award Position</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><a href="#">Work 1</a></td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                </tr>
                                <tr>
                                    <td><a href="#">Work 2</a></td>
                                    <td>Moe</td>
                                    <td>mary@example.com</td>
                                </tr>
                                <tr>
                                    <td><a href="#">Work 3</a></td>
                                    <td>Dooley</td>
                                    <td>july@example.com</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
