@extends('master')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script>
        Dropzone.options.videoUpload = {
            dictDefaultMessage: 'Drop here bitch',
            maxFiles: 1,
            success(file, res) {
                alert('Upload successfully');
                $("#time").val(res.videoTime);
                $("#video").val(res.videoUrl);
                $("#images").val(res.imageUrl);
            }
        }
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card card-body">
                    @include('auth.errors')
                    <div class="img card-img-top">
                        <img width="100" height="100" class="width-25p" src="{{ url($video->images) }}" alt="">
                        <form id="videoUpload" action="{{ route('panel.video.upload') }}"
                              class="dropzone width-75p float-right">
                            @csrf
                        </form>
                    </div>
                    <!--صهاهاقهصاهاشهاا-->
                    <!--these are for todo-->
                    <form method="post" action="{{route('video.update',['id'=>$video->id])}}">
                        <input type="hidden" value="" name="video" id="video">
                        @csrf
                        <input type="hidden" value="" id="time" name="time">
                        <input type="hidden" value="" id="images" name="images">
                        {{ method_field('patch') }}
                        <div class="form-group">
                            <label for="title">Title: </label>
                            <input type="text" id="title" value="{{ old('title',$video->title) }}" name="title"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="title">Custom Slug: </label>
                            <input type="text" id="slug" value="{{ $video->slug }}" name="slug" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags (seprate them with comma (,)): </label>
                            <input type="text" id="tags" value="{{ $video->tags }}" name="tags" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="tags">does users Can comment: </label>
                            <label for="">Can comment<input type="radio"
                                                            {{ $video->can_comment == 0 ? 'checked': ''}} id="can_comment"
                                                            name="can_comment" value="0"
                                                            class="form-check-inline"></label>
                            <label for="">Can't comment<input type="radio" id="can_comment"
                                                              {{ $video->can_comment == 1 ? 'checked': ''}}  name="can_comment"
                                                              value="1" class="form-check-inline"></label>
                        </div>

                        <div class="form-group">
                            <label for="description">description: </label>
                            <textarea type="text" id="description" name="description"
                                      class="form-control">{{ $video->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger">Submit data</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-body">
                    <h2 class="h4">
                        <p>This is a simple test for this page</p>
                    </h2>
                    <ul class="list-group">
                        @foreach($videos as $vid)
                            <a href="{{ url("panel/video/{$vid->id}") }}"><li class="p-1 list-group-item list-group-item-action"><img width="50px" height="50px" src="{{ url($vid->images) }}" class="br-10 mr-1 d-inline" alt="">{{ $vid->title  }}</li></a>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>


@endsection
