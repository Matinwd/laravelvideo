@extends('master')
@section('content')
    <div class="container mt-3">
        <div class="row">
{{--            @dd(auth()->user())--}}
            @foreach($allFollowings as $item)
                <div class="box-shadow-light col-4 card card-body mb-3 p-2">
                    <a href="{{ url("panel/video/$item->title") }}"></a>
                    <a class="mt-3" href="{{ url("panel/video/$item->id") }}"><h2
                            class="card-title text-dark d-inline">{{ $item->title }}</h2></a>
                    <div class="card-text">
                        <p>{{ mb_substr($item->description,0,20) . '....' }}</p>
                        <a href="{{ url("panel/video/{$item->id}") }}" class="btn btn-dark">Read the Doc</a>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
