@extends('master')
@section('content')
    <div class="container mt-3">
        <div class="row">
            @foreach($allUrPosts as $item)
                <div class="box-shadow-light col-4 card card-body mb-3 p-2  ">
                    <a href="{{ url("panel/video/$item->id") }}"><img class="card-img-top d-inline"
                                                                      src="{{ url("{$item->images}") }}"
                                                                      alt="{{ $item->title }}"></a>
                    <a class="mt-3" href="{{ url("panel/video/$item->id") }}"><h2
                            class="card-title text-dark d-inline">{{ $item->title }}</h2></a>
                    <div class="card-text">
                        <p>{{ mb_substr($item->description,0,20) . '....' }}</p>
                        <a href="{{ url("panel/video/{$item->id}") }}" class="btn btn-dark">Read the Doc</a>
                        @can('st_video',$item)
                            <form class="form-group mt-1" method="post" action="{{ url()->to("panel/video/$item->id") }}">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger" type="submit">Delete</button>
                                <a href="{{ url()->to("/panel/video/{$item->id}") }}" class="btn btn-info">Edit</a>
                            </form>
                            <p></p>
                        @endcan
                    </div>
                </div>
            @endforeach
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.0.6/sweetalert.min.js"></script>

    </div>
@stop
