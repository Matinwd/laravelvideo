@extends('master')
@section('content')
    @include('Components.Swal')
    <div class="container-fluid bg-theme">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="img">
                    <a href="#">
                        <img height="200" width="200" class="figure-img p-3 br-50"
                             src="/images/profile.jpg" alt="">
                    </a>
                    <span class="ml-2 text-white width-100 text-center font-size-30">{{ $channel->title }}</span>
                    <div class="btn-group-lg m-3">
                        @can('follow',$channel)
                            <a href="{{ route('follow.channel',$channel->title)  }}" class="btn btn-lg btn-primary">Follow {{ $channel->follow }}</a>
                        @else
                            @can('authorUnfollow',$channel)
                                <a href="{{ route('unfollow.channel',$channel->title)  }}"
                                   class="btn btn-lg btn-primary">unFollow {{ $channel->follow }}</a>
                            @endcan
                        @endcan
                        <a href="{{ $channel->website }}" class="btn btn-lg btn-info">WebSite</a>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link {{ (url()->current() == url("u/{$channel->user->name}") ?  'list-group-item-action': '' ) }}"
                       href="{{ route('channel',['name'=>$channel->user->name]) }}">Latest</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ (url()->current() == url("u/{$channel->user->name}/mostview") ?  'list-group-item-action': '' ) }}"
                       href="{{ route('channel.mostview',['name'=>$channel->user->name]) }}">Most viewed</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ (url()->current() == url("u/{$channel->user->name}/mostlike") ?  'list-group-item-action': '' ) }}"
                       href="{{ route('channel.mostlike',['name'=>$channel->user->name]) }}">Most Liked</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ (url()->current() == url("u/{$channel->user->name}/mostlike") ?  'list-group-item-action': '' ) }}"
                       href="{{ route('channel.mostlike',['name'=>$channel->user->name]) }}">Followers</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row m-3">
            @foreach($videos as $item)
                <div class="col-md-2 text-center">
                    <div class="card-body">
                        <div class="position-relative">
                            <a href="{{ url("p/{$item->slug}") }}"><img width="100%" class="box-shadow-light" height="100%" src="{{ url($item->images) }}" alt="">
                                <time
                                    class="position-absolute top-0 left-0 bg-info text-white p-1">{{ $item->time }}</time>
                            </a>
                            <a href="{{ url("p/{$item->slug}") }}"><h4 class="text-body">{{ mb_substr($item->title,0,10) }}</h4></a>
                            <p>{{ mb_substr($item->description,0,20) . '...' }}</p>
                            @can("st_video",$item)
                                <form method="post" action="{{ url()->to("panel/video/{$item->id}") }}">
                                    @csrf
                                    {{ method_field("DELETE") }}
                                    <a href="{{url("panel/video/{$item->id}")}}" class="btn btn-info">Edit</a>
                                    <button type="submit" class="btn btn-danger">delete</button>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
@section('content')
@stop
