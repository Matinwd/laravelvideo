@extends('master')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-group mt-5">
                    <li class="list-group-item-primary list-group-item">Last videos of this channel!</li>
                    @foreach($lastVideos as $video)
                        <a href="{{ url("p/{$video->slug}") }}" class="">
                            <li class="p-1 list-group-item-action list-group-item"><img width="50"
                                                                                        src="{{ url($video->images) }}"
                                                                                        alt=""><span
                                    class="p-1">{{ $video->title }}</span></li>
                        </a>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-7 offset-1">
                <p class="m-1">video from
                    <a href="{{ url()->to("/u/{$video->getUserName()}") }}">
                        <span class="text-info">{{ $video->channel->title }}</span>
                    </a>
                </p>
                <video id="example_video_1" class="video-js vjs-default-skin" style="width:100%;height:310px;"
                       controls poster="{{ url()->to("{$video->images}") }}"
                       data-setup='{"example_option":true}'>
                    <source src="{{ url()->to("{$video->video}") }}" type="video/mp4"/>
                </video>
                @include('Components.Swal')
                <div class="vid-text mt-3">
                    <div class="text-left">
                        <span class="text-primary">{{ $video->title }}</span>
                        <p class="">{{ $video->description }}</p>
                    </div>
                    <form action="">
                        <span class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i><span class="view"> : {{ $video->view_count }}</span></span>
                        @auth()
                            @can('canLike',$video)
                                <a href="{{ url("/p/{$video->slug}/like") }}" class="btn btn-success"
                                   type="submit"><i class="fa fa-heart-o mr-1" aria-hidden="true"></i>{{ $video->likes }}</a>
                            @endcan
                            @cannot('canLike',$video    )
                                <a href="{{ url("/p/{$video->slug}/dislike") }}" class="btn btn-success"
                                   type="submit"><i class="fa fa-heart" aria-hidden="true"></i> {{ $video->likes }}</a>
                            @endcannot
                        @endauth
                        @guest()
                            <a href="{{ url(route('login')) }}" class="btn btn-success"
                               type="submit"><i class="fa fa-heart-o" aria-hidden="true"></i> {{ $video->likes }}</a>
                        @endguest
                        <p class="d-inline">
                        </p>
                        <a class="btn btn-danger" href="{{ url("p/{$video->slug}/download") }}"><i class="fa fa-download" aria-hidden="true"></i> Download</a>

                        <div class="d-inline float-r img-likers">
                            @foreach($likers as $liker)
                                <img width="50" class="br-50 img-thumbnail" src="{{ $liker->pic }}" alt="tat" title="dwa">
                            @endforeach
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.15.0/videojs-contrib-hls.min.js"></script>
    <script>
        $(".video-js").one("click", function () {
            let Data = new FormData();
            let slugVideo = "{{ $video->slug }}";
            console.log(Data.keys());
            console.log(Data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/p/" + slugVideo + "/viewplus",
                type: "POST",
                data: Data,
                processData: false,
                contentType: false,
                success: function (response) {
                    console.log(response);
                    $(".view").text(" : "+response);
                }
            });

        });
    </script>
@stop
