<nav class="navbar navbar-expand-md navbar-dark bg-primary">
    <a class="navbar-brand" href="{{ url("") }}">LaraVideo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active pr-3">
                <a class="nav-link btn btn-outline-success" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item pr-3">
                <a class="nav-link btn btn-outline-light" href="#">Categories</a>
            </li>
            <li class="nav-item pr-3">
                <a class="nav-link btn btn-outline-info" href="#">Top videos</a>
            </li>
            <li class="nav-item pr-3">
                <a class="nav-link btn btn-outline-danger" href="#">Breaking news</a>
            </li>
            <li class="nav-item pr-3">
                <a class="nav-link btn btn-outline-danger" href="#">About us</a>
            </li>
        </ul>
    </div>
</nav>
