<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="{{ asset('js/app.js') }}"></script>
