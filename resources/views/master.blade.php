<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>LaraVideo</title>
    @include('sections.header')
</head>
<body>
@include('sections.navigation')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @yield('content', 'Default Content')
            @include('sections.footer')
        </div>
    </div>
</div>
</body>
</html>
